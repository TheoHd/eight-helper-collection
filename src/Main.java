import com.eight.commons.JSONHelper;
import org.json.JSONArray;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) {
        try {
            JSONArray joArr = JSONHelper.getJsonArray(JSONHelper.getJsonObject("test.json"), "horizonPaie");
            for (int i = 0; i < joArr.length(); i++) {
                String condition = joArr.getJSONObject(i).getString("condition");
                String field = joArr.getJSONObject(i).getString("field");
                String tableNamePattern = joArr.getJSONObject(i).getString("tableNamePattern");
                String value = joArr.getJSONObject(i).getString("value");
                System.out.println(condition);
                System.out.println(field);
                System.out.println(tableNamePattern);
                System.out.println(value);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
