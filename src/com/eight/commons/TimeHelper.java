package com.eight.commons;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeHelper {

    public static String currentTimeIn(String str) {
        if (str.equals("FR"))
            return "[" + LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + "] ";
        return "";
    }

}
