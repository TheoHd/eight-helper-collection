package com.eight.commons;

import java.io.*;;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileHelper {

    public static void createFile(String fileName, String value) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileOutputStream(
                    new File(fileName),
                    true));
        } catch (IOException e) {
            System.out.println("Couldn't create log file.");
            e.printStackTrace();
        }

        if (writer != null) {
            writer.println(value);
            writer.close();
        }
    }

    public static void appendToLog(String logFileName, String s) {
        FileHelper.createFile(logFileName, s);
    }

    public static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            assert is != null;
            is.close();
            assert os != null;
            os.close();
        }
    }

    public static void replaceInFile(String finalPathName, String[][] toReplace) throws IOException {
        Path path = Paths.get(finalPathName);
        Charset charset = StandardCharsets.UTF_8;
        String content = Files.readString(path, charset);
        for (String[] strings : toReplace) content = content.replace(strings[0], strings[1]);
        Files.write(path, content.getBytes(charset));
    }
}
