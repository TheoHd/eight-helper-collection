package com.eight.commons;

public class DisplayHelper {
    private String language;

    public DisplayHelper(String language) {
        this.language = language;
    }

    public void print(String s) {
        System.out.println(TimeHelper.currentTimeIn(language) + s);
    }


    public void print(String s, Runnable runnable) {
        System.out.print(TimeHelper.currentTimeIn(language) + s);
        runnable.run();
        System.out.println("OK");
    }
}
