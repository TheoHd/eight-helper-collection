package com.eight.commons;

import java.time.Duration;
import java.time.Instant;

public class ExecutionTimeHelper {

    private static Instant start;

    public static void start() {
        start = Instant.now();
    }

    public static void end() {
        Duration duration = Duration.between(start, Instant.now());
        System.out.println("Process finished with execution time of: " + duration.toString().split("PT")[1].split("S")[0] + " seconds.");
    }

    public static String endAndReturn() {
        Duration duration = Duration.between(start, Instant.now());
        return "Process finished with execution time of: " + duration.toString().split("PT")[1].split("S")[0] + " seconds.";
    }
}