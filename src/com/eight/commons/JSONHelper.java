package com.eight.commons;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;

public class JSONHelper {

    private static String[] getJsonArray(JSONObject jo) {
        String[] result = new String[jo.length()];
        for (int i = 1; i <= jo.length(); i++)
            result[i - 1] = JSONHelper.getString(jo, i);
        return result;
    }

    public static String getString(JSONObject jo, int i) {
        return jo.getString(String.valueOf(i));
    }

    public static JSONObject getJsonObject(String filePath, String objectPath) throws FileNotFoundException {
        String[] objectList = objectPath.split(",");
        JSONObject temp;
        try {
            temp = extractJsonObject(filePath, objectList);
            if (temp != null)
                return temp;
            else
                throw new FileNotFoundException("Couldn't find the getJsonObject.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        throw new FileNotFoundException("Couldn't find the file.");


    }

    private static JSONObject extractJsonObject(String filePath, String[] objectList) throws FileNotFoundException {
        JSONObject temp = null;
        JSONObject obj = JSONHelper.getJsonObject(filePath);
        for (int i = 0; i < objectList.length; i++) {
            if (i == 0)
                temp = obj.getJSONObject(objectList[i]);
            else
                temp = temp.getJSONObject(objectList[i]);
        }
        return temp;
    }

    public static String getString(String filePath, String objectPath, String string) throws FileNotFoundException {
        String[] objectList = objectPath.split(",");
        String result = null;
        JSONObject temp;
        try {
            temp = extractJsonObject(filePath, objectList);
            if (temp != null)
                result = temp.getString(string);
            else
                throw new FileNotFoundException("Couldn't find the getJsonObject.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (result != null)
            return result;
        else
            throw new FileNotFoundException("Couldn't find the file.");

    }

    public static JSONArray getJsonArray(JSONObject jo, String key){
        return jo.getJSONArray(key);
    }

    public static JSONObject getJsonObject(String filepath) throws FileNotFoundException {
        JSONObject result = null;
        File f = new File(filepath);
        if (f.exists()) {
            InputStream is = null;
            try {
                is = new FileInputStream(filepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String jsonTxt = null;
            try {
                jsonTxt = IOUtils.toString(is, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (jsonTxt != null)
                result = new JSONObject(jsonTxt);
            else
                throw new FileNotFoundException("Couldn't find the file.");
        }
        if (result != null)
            return result;
        throw new FileNotFoundException("Couldn't find the file.");
    }

    public static String[] getStringArray(String filePath, String objectPath) {
        String[] values = new String[0];
        try {
            values = JSONHelper.getJsonArray(JSONHelper.getJsonObject(filePath, objectPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return values;
    }

    public static String[] getStringArray(JSONArray ja){
        String[] res = new String[ja.length()];
        for(int i = 0 ; i < ja.length(); i++){
            res[i] = ja.get(i).toString();
        }
        return res;
    }
}
