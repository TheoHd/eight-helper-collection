package com.eight.commons;

public class StringHelper {
    public static String inlineValuesOf(String[] values) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.length; i++)
            sb.append(values[i]).append((i == values.length - 1) ? "" : ", ");
        return sb.toString();

    }
}
