# eight-helper-collection

This repository contains some tools to ease Java development projects.

For example a JSON file named ```settings.json``` containing this :

```json
{
  "namelist": {
    "1": "Théo",
    "2": "Baptiste",
    "3": "Valentin",
    "4": "Yannick"
  }
}
```

Can be accessed by simply writing this :

```java
String[] namelist = JSONHelper.getStringArray("settings.json", "namelist"); // contains Théo, Baptiste, Valentin and Yannick
```

Strings can also be accessed by writing this:

```java
String nameThree = JSONHelper.getStringArray("settings.json","namelist","3"); // contains Valentin
```

## Another example

```json
{
  "data": {
    "moredata": {
      "1":"Théo"
    }
  }
}
```
Firstname can be accessed by doing this:
```java
String name = JSONHelper.getStringArray("settings.json","data,moredata","1"); // contains Théo
```

## List handle

```json
{
  "data": [
    "Théo",
    "Yannick"
    "Valentin",
    "Baptiste"    
  ]
}
```

Data can be converted as a string list with the following code :

```java
String[] joArr = JSONHelper.getStringArray(JSONHelper.getJsonArray(JSONHelper.getJsonObject("test.json"), "horizonPaie"));
```

And then display it :

```java
for(String elem : joArr)
  System.out.println(elem);
```

Thus displaying :

```shell
Théo
Yannick
Valentin
Baptiste
```
